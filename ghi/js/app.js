
function createCard(name, description, pictureUrl, starts, ends, locationName) {
  return `
    <div class="col-md-4 md-4">
      <div class="card h-100 showdow rounded">
        <img src="${pictureUrl}" class="mh-25">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
      </div>
      <div class="card-footer text-body-secondary">
      ${starts} - ${ends}
      </div>
    </div>
  </div>
  `;
}


console.log("hello")

// Adding Event Listener for DOMContentLoaded
window.addEventListener('DOMContentLoaded', async () => {
  
  // API URL Declaration
  // The script sets the URL of the API that provides conference data.
    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      // Fetching Conference List Data
      const response = await fetch(url);
  
      if (!response.ok) {
        // method 1 to catch error 
        // var alertList = document.querySelectorAll('.alert');
        // var alerts = [].slice.call(alertList).map(function (element) {
        //   return new bootstrap.Alert(element);
        // });

        // method 2 to catch error
        var myAlert = document.getElementById('myAlert')
        myAlert.addEventListener('closed.bs.alert', function () {
        // do something, for instance, explicitly move focus to the most appropriate element,
        // so it doesn't get lost/reset to the start of the page
        // document.getElementById('...').focus()
        })

      } else {

        //If the response is successful, it parses the JSON body of the response to a JavaScript object (response.json())
        const data = await response.json();

        for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        // Detailed Response Handling:
        if (detailResponse.ok) {
          const details = await detailResponse.json();

          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const locationName=details.conference.location.name;

          const startDate = details.conference.starts; // Corrected variable name
          const startDateParsed = Date.parse(startDate);
          console.log(startDateParsed);
          let s = new Date(startDateParsed); // Corrected constructor name
          console.log(s);
          const starts = s.toLocaleDateString();

          const endDate = details.conference.ends; // Corrected variable name
          const endDateParsed = Date.parse(endDate);
          let e = new Date(endDateParsed);
          const ends = e.toLocaleDateString();

          const html = createCard(title, description, pictureUrl, starts, ends, locationName) 

          const row = document.querySelector('.row');
          row.innerHTML += html;

        }
      }
      }
    } catch (e) {
      console.error(e);
      // Figure out what to do if an error is raised
    }
  
  });