import React, { useEffect } from 'react';

function ConferenceForm() {
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('conference');
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.id;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
    
      return(
        <p>A conference form</p>
      )
    }
    
    export default ConferenceForm;