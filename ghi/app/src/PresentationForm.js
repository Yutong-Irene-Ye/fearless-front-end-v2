import React, { useState, useEffect } from 'react';

const PresentationForm = () => {
    const [states, setStates] = useState([]);
    const [presenterName, setPresenterName] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conferences, setConferences] = useState([]);
    const [selectedConference, setSelectedConference] = useState('');
    
    const handlePresenterNameChange = (event) => {
        setPresenterName(event.target.value);
      };
      
      const handleCompanyNameChange = (event) => {
        setCompanyName(event.target.value);
      };
      
      const handlePresenterEmailChange = (event) => {
        setPresenterEmail(event.target.value);
      };
      
      const handleTitleChange = (event) => {
        setTitle(event.target.value);
      };
      
      const handleSynopsisChange = (event) => {
        setSynopsis(event.target.value);
      };
      
      const handleConferencesChange = (event) => {
        setSelectedConference(event.target.value);
      };
      
      const handleSubmit = async (event) => {
        event.preventDefault();
      

        const data = {
          presenterName: presenterName,
          companyName: companyName,
          presenterEmail: presenterEmail,
          title: title,
          synopsis: synopsis,
          conferences: conferences
        };
      
        const conferenceUrl = 'http://localhost:8000/api/presentations/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      setPresenterName('');
      setCompanyName('');
      setPresenterEmail('');
      setTitle('');
      setSynopsis('');
      setConferences('');

    }
  }

    const fetchData = async () => {
        console.log([1,2,3])
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
        
        }
    
    }


  useEffect(() => {
    fetchData();
  }, []);


return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handlePresenterNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresenterEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange}  placeholder="Description" required id="synopsis" rows="3" name="synopsis" className="form-control"/>
              </div>
              <div className="mb-3">
              <select onChange={handleConferencesChange} required name="conferences" id="conference" className="form-select" >
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                        return (
                            <option key={conference.href} value={conference.href}>
                                {conference.name}
                           </option>

                        );
                        })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
export default PresentationForm;